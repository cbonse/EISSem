r"Gate-Classes for use as nodes"
from copy import deepcopy

class Gate(object):
    r"Base-class for all Gates. \
    Logical functions \"hidden\" in schemes of the subclasses."
    def __init__(self, name, inputs, outputs):
        self.name = name
        self.inputs, self.outputs = {}, {}
        for pin_name in inputs:
            self.inputs[pin_name] = "X"
        if isinstance(outputs, list):
            for pin_name in outputs:
                self.outputs[pin_name] = "X"
        else:
            self.outputs[outputs] = "X"
        self.parents, self.children = [], []
        self.consistent = False
        self.already_tried_schemes = []
        self.add_schemes()


    def choose_scheme(self, value):
        if value in ["D", "D-", "1", "0", "X"]:
            return deepcopy(self.schemes[value])
        else:
            raise ValueError

    def set_propagation_scheme(self, fault):
        self.untried_propagation_scheme = self.choose_scheme(fault)

    def set_justification_scheme(self):
        value = self.unique_output_value()
        self.untried_justification_scheme = self.choose_scheme(value)

    def update(self, scheme, propagation=None):
        for value, name in zip(scheme, self.inputs):
            self.inputs[name] = value

        if propagation:
            for name in self.outputs:
                self.outputs[name] = self.logical_output()

        for child in self.children:
            for name in self.inputs:
                if name in child.outputs:
                    child.outputs[name] = self.inputs[name]
        for parent in self.parents:
            for name in self.outputs:
                if name in parent.inputs:
                    parent.inputs[name] = self.outputs[name]
        self.consistent = True

    def unique_output_value(self):
        value = set(self.outputs.values())
        if len(value) == 1:
            return list(self.outputs.values())[0]
        else:
            print("Different Output-values for gate: ", self)
            raise ValueError

    def __repr__(self):
        repr = self.name+':\tInputs: '
        for key in self.inputs:
            repr += str(key) + ": "+str(self.inputs[key])+"\n\t\t\t"
        repr = repr[:-3]
        repr += "\t\tOutputs:"
        for key in self.outputs:
            repr += str(key) + ": "+str(self.outputs[key])+"\n\t\t\t"
        return repr

    def set_parent(self, gate):
        self.parents.append(gate)

    def set_child(self, gate):
        self.children.append(gate)

    def logical_output(self):
        r"""Standard-output, only overridden by NAND and NOR"""
        if 'D-' in self.inputs.values():
            return 'D-'
        else:
            return 'D'
