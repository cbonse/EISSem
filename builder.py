from gates.and_gate import AND
from gates.nand_gate import NAND
from gates.or_gate import OR
from gates.nor_gate import NOR
from gates.xor_gate import XOR
from gates.inv_gate import INV
from net_parser import parse_input

def link_gates(gate_list):
    r"""Links Gate by appending them mutually to their respective \
    children / parents lists.
    """
    for child in gate_list:
        output_pins = list(child.outputs)
        for out in output_pins:
            for parent in gate_list:
                if out in list(parent.inputs):
                    child.set_parent(parent)
                    parent.set_child(child)



def build_network(path_to_netlist):
    net_dict = parse_input(path_to_netlist)
    input_nodes, output_nodes = [], []
    gate_list, node_list = [], []
    for node in net_dict:
        name = node.lower().strip()
        values = net_dict[node]
        gate_type = net_dict[node][-1].lower().strip()
        if name.startswith('input'):
            input_nodes = net_dict[node]
        if name.startswith('output'):
            output_nodes = net_dict[node]
        if gate_type.startswith('and'):
            gate_list.append(AND(name, values[:-2], values[-2]))
        if gate_type.startswith('nand'):
            gate_list.append(NAND(name, values[:-2], values[-2]))
        if gate_type.startswith('or'):
            gate_list.append(OR(name, values[:-2], values[-2]))
        if gate_type.startswith('nor'):
            gate_list.append(NOR(name, values[:-2], values[-2]))
        if gate_type.startswith('xor'):
            gate_list.append(XOR(name, values[:-2], values[-2]))
        if gate_type.startswith('inv'):
            gate_list.append(INV(name, values[:-2], values[-2]))
        node_list.extend(values[:-1])

    assert len(input_nodes) > 0, 'No input-nodes supplied (Use \'input\'-keyword '+\
                                 'in netlist-file.)'
    assert len(output_nodes) > 0, 'No output-nodes supplied (Use \'output\'-keyword '+\
                                 'in netlist-file.)'
    assert len(gate_list) > 0, 'No nodes supplied...wrong netlist-file?'
    link_gates(gate_list)
    return gate_list, sorted(set(node_list)), input_nodes, output_nodes

