r"""Parser for netlists"""
import os
import re


def parse_input(path_to_file):
    SPICE_FILES = [".net", ".cir", ".ckt"]
    VHDL_FILES = [".vhd", ".vhdl"]
    if os.path.exists(path_to_file) and os.path.isfile(path_to_file):
        filetype = os.path.splitext(path_to_file)[1]
        if filetype in SPICE_FILES:
            return parse_spice(path_to_file)
        elif filetype in VHDL_FILES:
            return parse_vhdl(path_to_file)
        else:
            raise ValueError("File-type could not be resolved: ",filetype)


def parse_vhdl(path_to_file):
    r"""Extracts netlist from VHDL-files, by extracting the name of the used components
    and their mappings from the "port map"-statement:

        EXAMPLE_NAME: entity_name port map (input1=>NODE1, input2=>NODE2, ... )

    Second check for name neccessary since for example the "begin" statement is
    not followed by a semicolon and is included in the component string
    """
    with open(path_to_file) as f:
        content = "".join([el for el in f.readlines() if not el.startswith("-")])
    content = content.replace("\n", "").split(";")
    mapping = []
    for line in content:
        if "port" in line and "map" in line:
            mapping.append(line)
    netlist = {}
    for component in mapping:
        name = component[:component.find(":")].split(" ")
        name = [el for el in name if el][-1]
        mapping = component[component.find("(")+1:component.find(")")].split(",")
        mapping = [el.strip() for el in mapping]
        if "=>" in mapping[0]:
            mapping = [el[el.find("=>")+2:].strip() for el in mapping]
        netlist[name.lower()] = mapping
    return netlist

def check_line_spice(raw_line):
    r"""Simple helper-function for SPICE-Syntax"""
    line = raw_line.strip().lower().replace("\n", "")
    if not line or line.startswith("*"):
        return []
    else:
        return line


def parse_spice(path_to_file):
    r"""Extracts netlist from SPICE-netlist files.
    Currently, Parameters in Brackets have to be the last Parameter
    in their line.
    """
    content = []
    with open(path_to_file) as f:
        next(f)
        for line in f:
            valid_line = check_line_spice(line)
            if valid_line:
                content.append(valid_line)

    if '.control' in content:
        start = content.index(".control")
        end = content.index(".endc")+1
        content = content[0:start]+content[end:]

    content = [line for line in content if not line.startswith(".")]
    netlist = {}
    for line in content:
        if "(" in line:
            match = re.search(r"[a-zA-Z]*\(.*\)", line)
            parameter = line[match.start():match.end()]
            beginning = line[:match.start()]
            line = [el for el in beginning.split(" ") if el]
            line.append(parameter)
        else:
            line = [el for el in line.split(" ") if el]
        netlist[line[0].lower()] = line[1:]
    return netlist
